class Person:
    def __init__(self,fname, lname, age):        
        if fname == "" or lname == "" or age == "":
            raise Exception("Inputs cannot be empty")
        try:
            int(age)
        except:
            raise Exception("Age has to be a number")
        if int(age) > 0:
            self.fname = fname
            self.lname = lname
            self.age = age
        else:
            raise Exception("Age has to be bigger than zero")
        
    def str(self):
        return self.fname + " " + self.lname + ", " + self.age