from flask import Flask, request, render_template
from Address import Address
from Person import Person
from Student import Student

app = Flask(__name__)

addresses = []
people = []

@app.route("/")
def home():
    return render_template('home.html')

@app.route("/newaddress")
def new_address():
    return render_template('newaddress.html')

@app.route("/listaddresses")
def list_addresses():
    AddressList = "<ul>"

    for a in addresses:
        AddressList += f"<li>{a.str()}</li>"
    
    AddressList += "</ul><p><a href='/'>Return to Home</a></p>"

    return AddressList

@app.route("/newaddress", methods=['POST'])
def new_address_post():
    name = request.form['name']
    street = request.form['street']
    city = request.form['city']
    province = request.form['province']

    try:
        NewAddress = Address(name, street, city, province)
        addresses.append(NewAddress)
    except Exception as e:
        return f"<p>{e}</p><p><a href='/newaddress'>Previous Page</a></p>"
    
    return "<p>Added new address</p><p>" + NewAddress.str() + "<p/><p><a href='/'>Return to home</a></p>"

@app.route("/listpeople")
def list_people():
    PeopleList = "<ul>"

    for p in people:
        if isinstance(p, Student):
            PeopleList +=f"<li>{p.str()} (Student)</li>"
        else:
            PeopleList +=f"<li>{p.str()} (Person)</li>"

    PeopleList += "</ul><p><a href='/'>Return to Home</a></p>"

    return PeopleList

@app.route("/newperson")
def new_person():
    return render_template('newperson.html')

@app.route("/newperson", methods=['POST'])
def new_person_post():
    fname = request.form['first_name']
    lname = request.form['last_name']
    age = request.form['age']

    try:
        NewPerson = Person(fname, lname, age)
        people.append(NewPerson)
    except Exception as e:
        return f"<p>{e}</p><p><a href='/newperson'>Previous Page</a></p>"

    return "<p>Added new person</p><p>" + NewPerson.str() + "<p/><p><a href='/'>Return to home</a></p>"

@app.route("/newstudent")
def newstudent():
    return render_template('newstudent.html')

@app.route("/newstudent", methods=['POST'])
def newstudent_post():
    fname = request.form['first_name']
    lname = request.form['last_name']
    age = request.form['age']
    grade = request.form['grade']

    try:
        NewStudent = Student(fname, lname, age, grade)
        people.append(NewStudent)
    except Exception as e:
        return f"<p>{e}</p><p><a href='/newstudent'>Return</a></p>"

    return "<p>Added new student</p><p>" + NewStudent.str() + "<p/><p><a href='/'>Return to home</a></p>"