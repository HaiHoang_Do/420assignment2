from Person import Person

class Student (Person):
    def __init__(self, fname, lname, age, grade):
        if grade == "":
            raise Exception("Inputs cannot be empty")
        try:
            int(grade)
        except:
            raise Exception("Grade has to be a number")
        if int(grade) >= 0 and int(grade) <= 100:
            super().__init__(fname, lname, age)
            self.grade = grade
        else:
            raise Exception("Grade has to be between 0 and 100") 
        
    def str(self):
        return super().str() + ", " + self.map_grade()
    
    def map_grade(self):
        letter = ""

        if int(self.grade) <= 59:
            letter = "F"
        elif int(self.grade) <= 69 and int(self.grade) >= 60:
            letter = "D"
        elif int(self.grade) <= 79 and int(self.grade) >= 70:
            letter = "C"
        elif int(self.grade) < 95 and int(self.grade) >= 80:
            letter = "B"
        elif int(self.grade) >= 95:
            letter = "A"
            
        return letter
            
        